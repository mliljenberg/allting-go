#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

generateProto () {
  mkdir "$DIR"/"$1"
  protoc -I"$DIR"/../protofiles --go_out=plugins=grpc:"$DIR"/../protos/"$1" "$2".proto
#  protoc -I$DIR/../--go_out=plugins=grpc:/go/src/bitbucket.org/mliljenberg/allting-go/protos/$1 $2.proto
}

#mkdir /Users/marcusliljenberg/go/src/bitbucket.org/mliljenberg/allting-go/protos/

generateProto log_interface log_interface
generateProto analytics_log analytics_log
generateProto keywordspb keywords
generateProto transcribepb transcribe
generateProto write_dbpb write_db
generateProto loggerpb logger

