#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

generateProto () {
  mkdir $DIR/$1
  mkdir /Users/marcusliljenberg/go/src/gitlab.com/mliljenberg/allting-go/protos/$1
  protoc -I$DIR/../protofiles --go_out=plugins=grpc:$DIR/$1 $2.proto
  protoc -I$DIR/../protofiles --go_out=plugins=grpc:/go/src/gitlab.com/mliljenberg/allting-go/protos/$1 $2.proto
}

mkdir /Users/marcusliljenberg/go/src/gitlab.com/mliljenberg/allting-go/protos/

generateProto keywordspb keywords
generateProto transcribepb transcribe
generateProto write_dbpb write_db

