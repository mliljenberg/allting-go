module bitbucket.org/mliljenberg/allting-go/protos/write_dbpb

go 1.14

require (
	bitbucket.org/mliljenberg/allting-go/protos/log_interface v0.0.0-20201016155637-e8b11b453ce2
	github.com/golang/protobuf v1.4.3
	github.com/google/go-cmp v0.5.2 // indirect
	golang.org/x/net v0.0.0-20201010224723-4f7140c49acb // indirect
	golang.org/x/sys v0.0.0-20201016160150-f659759dc4ca // indirect
	golang.org/x/text v0.3.3 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/genproto v0.0.0-20201015140912-32ed001d685c // indirect
	google.golang.org/grpc v1.33.0
	google.golang.org/protobuf v1.25.0 // indirect
)
