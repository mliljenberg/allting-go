// Code generated by protoc-gen-go. DO NOT EDIT.
// source: logger.proto

package loggerpb

import (
	analytics_log "bitbucket.org/mliljenberg/allting-go/protos/analytics_log"
	log_interface "bitbucket.org/mliljenberg/allting-go/protos/log_interface"
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Response struct {
	Status               bool     `protobuf:"varint,1,opt,name=status,proto3" json:"status,omitempty"`
	ErrorCode            int32    `protobuf:"varint,2,opt,name=error_code,json=errorCode,proto3" json:"error_code,omitempty"`
	Message              string   `protobuf:"bytes,3,opt,name=message,proto3" json:"message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Response) Reset()         { *m = Response{} }
func (m *Response) String() string { return proto.CompactTextString(m) }
func (*Response) ProtoMessage()    {}
func (*Response) Descriptor() ([]byte, []int) {
	return fileDescriptor_d43b7bfc6b6f7b16, []int{0}
}

func (m *Response) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Response.Unmarshal(m, b)
}
func (m *Response) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Response.Marshal(b, m, deterministic)
}
func (m *Response) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Response.Merge(m, src)
}
func (m *Response) XXX_Size() int {
	return xxx_messageInfo_Response.Size(m)
}
func (m *Response) XXX_DiscardUnknown() {
	xxx_messageInfo_Response.DiscardUnknown(m)
}

var xxx_messageInfo_Response proto.InternalMessageInfo

func (m *Response) GetStatus() bool {
	if m != nil {
		return m.Status
	}
	return false
}

func (m *Response) GetErrorCode() int32 {
	if m != nil {
		return m.ErrorCode
	}
	return 0
}

func (m *Response) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func init() {
	proto.RegisterType((*Response)(nil), "logger.Response")
}

func init() { proto.RegisterFile("logger.proto", fileDescriptor_d43b7bfc6b6f7b16) }

var fileDescriptor_d43b7bfc6b6f7b16 = []byte{
	// 230 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x90, 0xb1, 0x4e, 0xc3, 0x30,
	0x10, 0x86, 0x31, 0xd0, 0x90, 0x1e, 0x20, 0x21, 0x47, 0x42, 0x51, 0x24, 0xa4, 0xa8, 0x03, 0xca,
	0x94, 0x01, 0x16, 0x56, 0x60, 0xcd, 0xe4, 0x11, 0x86, 0xc8, 0x4d, 0x8f, 0x53, 0x25, 0x93, 0x0b,
	0x3e, 0x77, 0xe0, 0x3d, 0x78, 0x60, 0xd4, 0xb8, 0x96, 0x3a, 0x74, 0xfc, 0x3f, 0xff, 0x3e, 0x7d,
	0x77, 0x70, 0xe3, 0x98, 0x08, 0x7d, 0x3b, 0x79, 0x0e, 0xac, 0xb3, 0x98, 0xaa, 0xc2, 0x31, 0xf5,
	0xdb, 0x31, 0xa0, 0xff, 0xb2, 0x03, 0xc6, 0xc7, 0xaa, 0xb0, 0xa3, 0x75, 0xbf, 0x61, 0x3b, 0x48,
	0xef, 0x98, 0x22, 0x5c, 0x7d, 0x42, 0x6e, 0x50, 0x26, 0x1e, 0x05, 0xf5, 0x3d, 0x64, 0x12, 0x6c,
	0xd8, 0x49, 0xa9, 0x6a, 0xd5, 0xe4, 0xe6, 0x90, 0xf4, 0x03, 0x00, 0x7a, 0xcf, 0xbe, 0x1f, 0x78,
	0x83, 0xe5, 0x79, 0xad, 0x9a, 0x85, 0x59, 0xce, 0xe4, 0x9d, 0x37, 0xa8, 0x4b, 0xb8, 0xfa, 0x46,
	0x11, 0x4b, 0x58, 0x5e, 0xd4, 0xaa, 0x59, 0x9a, 0x14, 0x9f, 0xfe, 0x14, 0x64, 0xdd, 0x6c, 0xa4,
	0x1f, 0x01, 0x3a, 0x26, 0x83, 0x3f, 0x3b, 0x94, 0xa0, 0x2f, 0xdb, 0x8e, 0xa9, 0xba, 0x6b, 0x0f,
	0xf2, 0xc9, 0x60, 0x75, 0xa6, 0x1b, 0xb8, 0xee, 0x98, 0x24, 0x15, 0x17, 0xfb, 0xa2, 0x9c, 0x6c,
	0xbe, 0x40, 0xf1, 0x9a, 0x16, 0x3a, 0x1a, 0x7d, 0xdb, 0x1e, 0xd3, 0x53, 0x3f, 0xdf, 0xe0, 0x23,
	0x8f, 0x70, 0x5a, 0xaf, 0xb3, 0xf9, 0x0c, 0xcf, 0xff, 0x01, 0x00, 0x00, 0xff, 0xff, 0x62, 0x93,
	0x8e, 0x28, 0x48, 0x01, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// LoggerClient is the client API for Logger service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type LoggerClient interface {
	LogRequest(ctx context.Context, in *log_interface.Log, opts ...grpc.CallOption) (*Response, error)
	LogsRequest(ctx context.Context, in *log_interface.Logs, opts ...grpc.CallOption) (*Response, error)
	AnalyticsLogRequest(ctx context.Context, in *analytics_log.AnalyticsLog, opts ...grpc.CallOption) (*Response, error)
}

type loggerClient struct {
	cc *grpc.ClientConn
}

func NewLoggerClient(cc *grpc.ClientConn) LoggerClient {
	return &loggerClient{cc}
}

func (c *loggerClient) LogRequest(ctx context.Context, in *log_interface.Log, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := c.cc.Invoke(ctx, "/logger.Logger/LogRequest", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *loggerClient) LogsRequest(ctx context.Context, in *log_interface.Logs, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := c.cc.Invoke(ctx, "/logger.Logger/LogsRequest", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *loggerClient) AnalyticsLogRequest(ctx context.Context, in *analytics_log.AnalyticsLog, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := c.cc.Invoke(ctx, "/logger.Logger/AnalyticsLogRequest", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// LoggerServer is the server API for Logger service.
type LoggerServer interface {
	LogRequest(context.Context, *log_interface.Log) (*Response, error)
	LogsRequest(context.Context, *log_interface.Logs) (*Response, error)
	AnalyticsLogRequest(context.Context, *analytics_log.AnalyticsLog) (*Response, error)
}

// UnimplementedLoggerServer can be embedded to have forward compatible implementations.
type UnimplementedLoggerServer struct {
}

func (*UnimplementedLoggerServer) LogRequest(ctx context.Context, req *log_interface.Log) (*Response, error) {
	return nil, status.Errorf(codes.Unimplemented, "method LogRequest not implemented")
}
func (*UnimplementedLoggerServer) LogsRequest(ctx context.Context, req *log_interface.Logs) (*Response, error) {
	return nil, status.Errorf(codes.Unimplemented, "method LogsRequest not implemented")
}
func (*UnimplementedLoggerServer) AnalyticsLogRequest(ctx context.Context, req *analytics_log.AnalyticsLog) (*Response, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AnalyticsLogRequest not implemented")
}

func RegisterLoggerServer(s *grpc.Server, srv LoggerServer) {
	s.RegisterService(&_Logger_serviceDesc, srv)
}

func _Logger_LogRequest_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(log_interface.Log)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(LoggerServer).LogRequest(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/logger.Logger/LogRequest",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(LoggerServer).LogRequest(ctx, req.(*log_interface.Log))
	}
	return interceptor(ctx, in, info, handler)
}

func _Logger_LogsRequest_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(log_interface.Logs)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(LoggerServer).LogsRequest(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/logger.Logger/LogsRequest",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(LoggerServer).LogsRequest(ctx, req.(*log_interface.Logs))
	}
	return interceptor(ctx, in, info, handler)
}

func _Logger_AnalyticsLogRequest_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(analytics_log.AnalyticsLog)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(LoggerServer).AnalyticsLogRequest(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/logger.Logger/AnalyticsLogRequest",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(LoggerServer).AnalyticsLogRequest(ctx, req.(*analytics_log.AnalyticsLog))
	}
	return interceptor(ctx, in, info, handler)
}

var _Logger_serviceDesc = grpc.ServiceDesc{
	ServiceName: "logger.Logger",
	HandlerType: (*LoggerServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "LogRequest",
			Handler:    _Logger_LogRequest_Handler,
		},
		{
			MethodName: "LogsRequest",
			Handler:    _Logger_LogsRequest_Handler,
		},
		{
			MethodName: "AnalyticsLogRequest",
			Handler:    _Logger_AnalyticsLogRequest_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "logger.proto",
}
