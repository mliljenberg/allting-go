module bitbucket.org/mliljenberg/allting-go/protos/loggerpb

go 1.14

require (
	bitbucket.org/mliljenberg/allting-go v0.0.0-20210702212345-7e9fc4f60250 // indirect
	bitbucket.org/mliljenberg/allting-go/protos/analytics_log v0.0.0-20210702212345-7e9fc4f60250
	bitbucket.org/mliljenberg/allting-go/protos/log_interface v0.0.0-20210702211640-977bb77c53ed
	github.com/golang/protobuf v1.5.2
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/genproto v0.0.0-20210701191553-46259e63a0a9 // indirect
	google.golang.org/grpc v1.39.0
)
